const express = require('express');
const serveStatic = require('serve-static');
const path = require('path')

app = express();
app.use(serveStatic(__dirname + "/dist"));
const port = 7777;


app.get('*', (req,res)=>{
  res.sendFile(path.join(__dirname, './dist', 'index.html'));
})

app.listen(55555, () => {
   console.log(`Server running at http://localhost:${7777}/`);
 });